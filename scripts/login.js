$(() => {
    $.when(setNotification("scripts/notification-too"))
    .done(() => {
        getDate()
        
        $('form').on('submit', function(e) {
            e.preventDefault()

            const user = $("#username").val()
            const pass = $("#password").val()

            const data = {
                user: user,
                pass: pass
            }

            if(user != "") {
                login(data)
            }
        })
    })
})

function login(data) {
    $.ajax({
        type: "POST",
        url: "./api/auth.php",
        data: jQuery.param({ login:data }),
        processData: false
    })
    .done(response => {
        let data = JSON.parse(response)

        if(data.type == "success") {
            window.location.href = "./"
        }
        else {
            showNotification(data.data, data.type, 50000)
        }
    })
}

function getDate() {
    $.ajax({
        type: "GET",
        url: "./api/income.php",
        data: jQuery.param({ getDate:"" }),
        processData: false
    })
    .done(response => {
        let data = JSON.parse(response)

        if(data.type == "success") {
            $("#date").html(data.data)
        }
        else {
            showNotification(data.data, data.type)
        }
    })
}